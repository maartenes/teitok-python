import lxml.etree as etree
import sys

filenames = {}

class TTObj:
	filename = ""
	def __init__(self, **opts):
		self.regions = {}
		self.tokens = []
		self.meta = {}
		xmlstring = "<TEI/>"	
		xmlf = etree.ElementTree(etree.fromstring(xmlstring))
		self.xml = xmlf
		
	# Make an object out of a range
	def makeobj(self, elm):
		xobj = {}
		for attr in elm.keys():
			xobj[attr] = elm[attr]
		xobj['tokens'] = []
		for idx in elm['idxs']:
			xobj['tokens'].append(self.tokens[idx])
		text = ""
		for tok in xobj['tokens']:
			text = text + tok['form'] + " "
		xobj['text'] = text

		return xobj
	

# Read an XML file as an ElementTree
# if no name is given, assume it is given on the command line
def readxml(xmlfile = "", **opts):
	if xmlfile == "":
		if len(sys.argv) == 1:
			print("Please provide an XML filename")
			exit()
		xmlfile = sys.argv[1]
	xmlf = etree.parse(xmlfile)
	filenames[xmlf] = xmlfile
	return xmlf

# Read an XML file into a TTOBJ
def read(xmlfile = "", **opts):
	if xmlfile == "":
		if len(sys.argv) == 1:
			print("Please provide an XML filename")
			exit()
		xmlfile = sys.argv[1]
	xmlf = readxml(xmlfile)

	# Go through tokens, we will deal with dtoks on-the-fly
	tokxp = "text//tok"

	ttobj = TTObj()
	tokens = []
	ttobj.xml = xmlf
	tokcnt = 0
	id2idx = {}
	for tok in xmlf.findall(tokxp):
		tokid = "tok-" + str(tokcnt+1)
		if "id" in tok.keys():
			tokid = tok.attrib['id']
		# Check if we need to do dtoks
		if tok.findall('dtok'):
			dcnt = 1
			for dtok in tok.findall('./dtok'):
				newtok = {}
				dtokid = "tok-" + str(tokcnt+1)
				if "id" in dtok.keys():
					dtokid = dtok.attrib['id']
				newtok['id'] = dtokid
				id2idx[dtokid] = tokcnt
				if "form" in dtok.keys():
					dword = dtok.attrib["form"]
				else:
					dword = '#'
				newtok['form'] = dword
				newtok['xml'] = dtok
				tokens.append(newtok)
				tokcnt = tokcnt + 1
		else:				
			newtok = {}
			newtok['id'] = tokid
			id2idx[tokid] = tokcnt
			tokcnt = tokcnt + 1
			newtok['xml'] = tok
			if "form" in tok.keys():
				word = tok.attrib["form"]
				if word == '--':
					word = None
			else:
				word = tok.text
			if word is not None: 
				newtok['form'] = word
				tokens.append(newtok)
	ttobj.tokens = tokens
	
	# Now add the regions
	for node in xmlf.findall("//*"):
		nn = node.tag
		ntoks = node.findall(".//tok")
		if nn not in ttobj.regions.keys():
			ttobj.regions[nn] = []
		if nn == "tok":
			type = "tokens"
		elif ntoks:
			newelm = {}		
			newelm['xml'] = node
			newelm['ids'] = []
			newelm['idxs'] = []
			for ntok in ntoks:
				if ntok.findall('dtok'):
					for dtok in tok.findall('./dtok'):
						nid = dtok.attrib['id']
						newelm['ids'].append(nid)
						newelm['idxs'].append(id2idx[nid])
				else:
					nid = ntok.attrib['id']
					newelm['ids'].append(nid)
					newelm['idxs'].append(id2idx[nid])
			ttobj.regions[nn].append(newelm)
		elif 'sameAs' in node.attrib:
			saar = node.attrib['sameAs'][1:].split(' #')
			newelm = {}		
			newelm['xml'] = node
			newelm['ids'] = saar
			newelm['idxs'] = []
			for id in saar:
				newelm['idxs'].append(id2idx[id])
			ttobj.regions[nn].append(newelm)
		elif not node.getchildren():
			type = "empty node"
		else:
			type = "node without tokens"

	ttobj.id2idx = id2idx
	return ttobj


# Write out an XML file
# if no name is given, write it back to where it was loaded from
def save(xmlf, filename=""):
	if filename == "":
		filename = filenames[xmlf]
	print("output written to " + filename)
	xmlf.write(filename,encoding="UTF-8")	
	
# Insert a node before/after another	
def insertbefore(node, newchild):
	idx = node.getparent().index(node)
	node.getparent().insert(idx,newchild)

def insertafter(node, newchild):
	idx = node.getparent().index(node)
	node.getparent().insert(idx+1,newchild)

# Move all items inside an new parent node, added as an empty node before
def moveinside(el1, el2 = None):
	if el1 is None:
		return
	tmp = el1.attrib['sameAs'].split(" ")
	lastid = tmp[-1][1:]
	nxt = el1.getnext()
	if el2 is not None:
		while nxt is not None and nxt != el2:
			el1.append(nxt)
			nxt = el1.getnext()
		el2.getparent().remove(el2)
	else:		
		# If no last element is given, move based on sameAs
		while nxt is not None and nxt.tag != el1.tag:
			el1.append(nxt)
			if "id" in nxt.attrib.keys() and nxt.attrib['id'] == lastid:
				break
			nxt = el1.getnext()

def obj2tei(obj, **opts):
	xmlstring = "<TEI/>"	
	xmlf = etree.ElementTree(etree.fromstring(xmlstring))
	ttheader = etree.Element("teiHeader")
	xmlf.getroot().append(ttheader)
	ttheader.tail = "\n"
	tttxt = etree.Element("text")
	xmlf.getroot().append(tttxt)
	selm = "s"
	if 'selm' in opts.keys():
		selm = opts['selm']
	
	for sent in obj:
		xsent = etree.Element(selm)
		tttxt.append(xsent)
		for attr in sent.keys():
			if attr != "tokens" and attr != "text":
				xsent.set(attr, sent[attr])
		for tok in sent['tokens']:
			xtok = etree.Element("tok")
			for attr in tok.keys():
				if attr == "form":
					xtok.text = tok[attr]
				else:
					xtok.set(attr, tok[attr])
			xsent.append(xtok)
			xtok.tail = " "
		xsent.tail = "\n"
	return xmlf
	
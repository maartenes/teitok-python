import lxml.etree as etree
from teitok import ttxml
import re
from datetime import date

# Write ttxml object as CoNLL-U 
def write(tt):
	snr = 0
	for sent in tt.regions['s']:
		snr = snr + 1
		if 'id' in sent['xml'].attrib:
			sentid = sent['xml'].attrib['id']
		else:
			sentid = 'sent-' + str(snr)
		sentid = sentid
		rng = range(sent['trng'][0],sent['trng'][1]+1)
		flds = "ord,word,lemma,upos,xpos,feats,ohead,deprel,deps,misc,id".split(",")
		ord = 1
		stxt = ''
		lines = []
		for i in rng:
			tok = tt.tokens[i]
			stxt += tok['form'] + ' '
			line = [str(ord), tok['form']]
			ord = ord + 1
			for att in flds:
				if att == 'ord' or att == 'word':
					continue
				if att in tok['xml'].attrib:
					val = tok['xml'].attrib[att]
				else:
					val = '_'
				line.append(val)
			tokid = line.pop()
			if tokid != '_' and not re.match(r".*tokId=([^|]+).*", line[9]):
				if line[9] == '_':
					line[9] = ''
				else:
					line[9] = line[9] + '|'
				line[9] = line[9] + 'tokId='+tokid					
			lines.append("\t".join(line))
		print("# sent_id = " + sentid)
		print("# text = " + stxt)
		print ("\n".join(lines))
		print()

# Read a CoNLL-U file into an (incomplete) ttxml object
def read(filename):
	file1 = open(filename, 'r')
	Lines = file1.readlines()
	flds = "ord,word,lemma,upos,xpos,feats,ohead,deprel,deps,misc".split(",")
  
	ttobj = ttxml.TTObj()
	ttobj.regions = {}
	ttobj.xml = etree.Element('TEI')
	ttobj.xml.text = '\n'
	
	today = date.today()
	headerxml = """<teiHeader>
	<revisionDesc>
		<change when=\""""+today.strftime("%Y-%m-%d")+"""\" who="TTSPACY">Created from CoNLL-U file """+filename+"""</change>
	</revisionDesc>
</teiHeader>"""
	header = etree.fromstring(headerxml)
	header.tail = '\n'
	ttobj.xml.append(header)

	teitext = etree.Element('text')
	teitext.text = '\n'
	teitext.tail = '\n'
	ttobj.xml.append(teitext)
	tokens = []
	sents = []
	sent = {}
	sent['xml'] = etree.Element('s')
	tcnt = 0 
	ss = 0
  	
	# Strips the newline character
	for line in Lines:
		line = line.strip()
		if line == '':
			sent['xml'].tail = '\n'
			sent['trng'] = [ss,tcnt-1]
			sents.append(sent)
			teitext.append(sent['xml'])
			sent = {}
			sent['xml'] = etree.Element('s')
		elif line[0] == '#':
			if re.match(r"# *sent_?id *= *([^|]+).*", line):
				sent['id'] = re.match(r"# *sent_?id *= *([^|]+).*", line).groups()[0]
		else:
			vals = line.split("\t")
			newtok = {}
			newtok['xml'] = etree.Element('tok')
			newtok['form'] = vals[1]
			if re.match(r".*tokId=([^|]+).*", vals[9]):
				tokid = re.match(r".*tokId=([^|]+).*", vals[9]).groups()[0]
				newtok['id'] = tokid
				newtok['xml'].set('id', tokid)			
			for i, fld in enumerate(flds):
				if vals[i] != '_' and fld != 'word':
					newtok['xml'].set(fld, vals[i])			
			tokens.append(newtok)
			if not re.match(r".*SpaceAfter=No.*", vals[9]):
				newtok['xml'].tail = ' '
			newtok['xml'].text = vals[1]
			sent['xml'].append(newtok['xml'])
			tcnt = tcnt + 1

	sent['xml'].tail = '\n'
	sent['trng'] = [ss,tcnt-1]
	sents.append(sent)
	teitext.append(sent['xml'])

	ttobj.tokens = tokens
	ttobj.regions['s'] = sents
	
	return ttobj
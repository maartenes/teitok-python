import lxml.etree as etree
import flair
from flair.data import Sentence

def makesents(tt):
	sentences = []
	for ttsent in tt.regions['s']:
		sentence = Sentence() 
		snr = snr + 1
		if 'id' in ttsent['xml'].attrib:
			sentid = ttsent['xml'].attrib['id']
		else:
			sentid = 'sent-' + str(snr)
		sentid = sentid
		rng = range(sent['trng'][0],sent['trng'][1]+1)
		tokcnt = 0
		for i in rng:
			tok = tt.tokens[i]
			for attr in tok.keys():
				val = tok[attr]
				if attr == "word":
					sentence.add_token(val)
				else:
					sentence[tokcnt].add_tag(attr, val)
			tokcnt = tokcnt + 1
		sentences.append(sentence)
	return sentences

def writeback(xmlf, sentences, attrs = ""):
	toklist = {}
	doatts = attrs.split(",")
	for tok in xmlf.findall("//tok"):
		tokid = tok.attrib['id']
		toklist[tokid] = tok
	for sentid in sentences:
		sentence = sentences[sentid]
		for token in sentence:
			tokid = token.get_tag('id').value
			xtok = toklist[tokid]
			for attr in doatts:
				val = token.get_tag(attr).value
				if val != "" and val != "_":
					xtok.attrib[attr] = val
	

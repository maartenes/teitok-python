import torch
import sys, getopt
import torchaudio
from datetime import date

import lxml.etree as etree
from teitok import ttxml

def recognize(audiofile = "", **opts):
	#$ resampler = torchaudio.transforms.Resample(new_freq=16_000)
	model_name = ""

	if "model" in opts.keys() and opts['model'] is not None:
		model_name = opts['model']
	elif model_name == "" and "lang" in opts.keys() and opts['lang'] is not None:
		lang = opts['lang']
		from huggingface_hub import HfApi, ModelFilter, ModelSearchArguments
		api = HfApi() 
		model_args = ModelSearchArguments()

		filt = ModelFilter(
			task = ["automatic-speech-recognition"],
			library = ["wav2vec2"],
			language = [lang],
		)
		model_list = api.list_models(filt)
		for model in model_list:
			model_name = model_list[0].modelId
			if not model.private:
				break
		# print ("Chosen model: " + model_name)

	today = date.today()

	torch.random.manual_seed(0)
	device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

	bundle = torchaudio.pipelines.WAV2VEC2_ASR_BASE_960H
	if "bundle" in opts.keys():
		bundle = opts['bundle']
		
	labels= bundle.get_labels()

	model = None
	if model_name != "":
		from transformers import Wav2Vec2ForCTC
		from torchaudio.models.wav2vec2.utils import import_huggingface_model
		original = Wav2Vec2ForCTC.from_pretrained(model_name)
		model = import_huggingface_model(original)
	else:
		model = bundle.get_model().to(device)
		model_name = "WAV2VEC2_ASR_BASE_960H"

	# print(model.__class__.__qualname__)

	audiofile = "Audio/song_cjrg_teasdale.wav"

	waveform, sample_rate = torchaudio.load(audiofile)
	waveform = waveform.to(device)

	if sample_rate != bundle.sample_rate:
		waveform = torchaudio.functional.resample(waveform, sample_rate, bundle.sample_rate)
	
	with torch.inference_mode():
		features, _ = model.extract_features(waveform)
		 
	with torch.inference_mode():
		emission, _ = model(waveform)

	num_channels, num_frames = waveform.shape
	audiotime = num_frames/bundle.sample_rate
	fps = len(emission[0])/audiotime

	if num_channels > 1:
		waveform = waveform[0]

	headerxml = """<teiHeader>
    <fileDesc>
      <sourceDesc>
        <recordingStmt>
          <recording type=\"audio\">
            <media url=\""""+audiofile+"""\"/>
          </recording>
        </recordingStmt>
	  </sourceDesc>
    </fileDesc>
	<revisionDesc>
		<change when=\""""+today.strftime("%Y-%m-%d")+"""\" who="TTWAV">Automatically recognized using """+model_name+"""</change>
	</revisionDesc>
</teiHeader>"""

	# Decode the emission into words 
	# assume same label twice to be as long label
	xobj = ttxml.TTObj()
	xmltext = etree.Element("text")
	teiheader = etree.fromstring(headerxml)
	xobj.xml.getroot().append(teiheader)
	xobj.xml.getroot().append(xmltext)
	fc = 0
	last = 0
	word = ""
	id1 = 0
	wc = 1
	stretch = 0.05
	if "stretch" in opts.keys():
		stretch = opts['stretch']

	for i in emission[0]:
		lbl = torch.argmax(i)
		val = i[lbl].item()
		tot = torch.sum(i).item()
		letter = labels[lbl]
		print (str(i) + " = " + str(lbl) + " => " + letter )
		# print(str(fc) + ": " + letter)
		if lbl == 0:
			# Blank
			blanked = 1
			if word == "":
				# Do not count interword frames
				id1 = fc
		elif lbl == 1:
			# Word separator
			if word != "":		
				word = word.lower()
				token = {}
				token['word'] = word
				# token['cert'] = wordprob
				token['xml'] = etree.Element("tok")
				token['xml'].tail = " "
				token['xml'].text = word
				xmltext.append(token['xml'])
				token['start'] = ( id1 / fps ) - stretch
				token['end'] = ( fc / fps ) + stretch
				token['xml'].attrib['start'] = "{:.3f}".format(token['start'])
				token['xml'].attrib['end'] = "{:.3f}".format(token['end'])
				xobj.tokens.append(token)
				word = ""
				wc = wc + 1
				token['id'] = "w-" + str(wc)
			id1 = fc
		elif lbl == last:
			duplicate = 1
		else:
			word = word + letter
		last = lbl
		fc = fc + 1
  
	# Add sentences   	
	selm = "u"
	xobj.regions[selm] = []  
	lasttok = None
	tok1 = xobj.tokens[0]
	tcnt = 0
	scnt = 1
	idx1 = 0
	longbreak = 0.5
	if "longbreak" in opts.keys():
		longbreak = opts['longbreak']
	for tok in xobj.tokens:
		if ( lasttok and tok['start']-lasttok['end'] > longbreak ) or tok == xobj.tokens[-1]:	
			if tok == xobj.tokens[-1]:
				lasttok = tok
				tcnt =  tcnt + 1
			scnt = scnt + 1
			sentence = {}
			id1 = tok1['id']
			if "capitalize" in opts.keys():
				tok1['xml'].text = tok1['word'].capitalize()
			id2 = lasttok['id']
			sentence['idng'] = [id1, id2]
			sentence['trng'] = [idx1, tcnt]
			sentence['id'] = selm + "-" + str(scnt)
			sentence['xml'] = etree.Element(selm)
			sentence['xml'].tail = "\n"
			sentence['start'] = tok1['start']
			sentence['end'] = lasttok['end']
			sentence['xml'].attrib['start'] = "{:.3f}".format(sentence['start'])
			sentence['xml'].attrib['end'] = "{:.3f}".format(sentence['end'])
			tokrng = xobj.tokens[sentence['trng'][0]:sentence['trng'][1]:]
			for token in tokrng:
				sentence['xml'].append(token['xml'])
			xmltext.append(sentence['xml'])
			xobj.regions[selm].append(sentence)
			tok1 = tok
			idx1 = tcnt
		lasttok = tok
		tcnt = tcnt + 1
	
	return xobj 


import spacy
import lxml.etree as etree
from teitok import ttxml
from spacy.tokens import Doc
from spacy.tokens import Token
from datetime import date

Doc.set_extension("sentid", default=None)
Token.set_extension("custom", default={})

udflds = "ord,form,lemma,upos,xpos,feats,ohead,deprel,deps,misc".split(",")
spacyflds = {
	"upos": "pos_",
	# "feats": "tag_",
	"reg": "norm_",
	"lemma": "lemma_",
	"ner": "ent_iob_",
	"deprel": "dep_",
}

# Convert a TTXML sentence object to a SpaCy Doc
def makedoc(tt):
	toklist = []
	sentstart = []
	for i, tok in enumerate(tt.tokens):
		toklist.append(tok['form'])
		sentstart.append(False)
	if 's' in tt.regions.keys():
		for sent in tt.regions['s']:
			sentstart[sent['idxs'][0]] = True

	# Allow IDs on Doc and Token - for which we need a dummy NLP model
	nlp = spacy.load("en_core_web_sm")
	spacydoc = Doc(nlp.vocab, toklist, sent_starts=sentstart)

	for i, tok in enumerate(tt.tokens):
		for attr in tok['xml'].keys():
			val = tok['xml'].attrib[attr]
			if attr in spacyflds.keys():
				if attr == "upos":
					spacydoc[i].pos_ = val
				if attr == "reg":
					spacydoc[i].norm_ = val
				if attr == "ner":
					spacydoc[i].ent_iob_ = val
				if attr == "lemma":
					spacydoc[i].lemma_ = val
				if attr == "deprel":
					spacydoc[i].dep_ = val
			else:
				spacydoc[i]._.custom[attr] = val
		tokid = tok['id']
		spacydoc[i]._.custom['id'] = tokid
	
	return spacydoc

def fromdoc(doc, options = {}):
  
	ttobj = ttxml.TTObj()
	ttobj.regions = {}
	ttobj.xml = etree.Element('TEI')
	ttobj.xml.text = '\n'

	today = date.today()
	headerxml = """<teiHeader>
	<revisionDesc>
		<change when=\""""+today.strftime("%Y-%m-%d")+"""\" who="TTSPACY">Created from SpaCy document</change>
	</revisionDesc>
</teiHeader>"""
	header = etree.fromstring(headerxml)
	header.tail = '\n'
	ttobj.xml.append(header)

	teitext = etree.Element('text')
	teitext.text = '\n'
	teitext.tail = '\n'
	ttobj.xml.append(teitext)
	tokens = []
	sents = []
	tcnt = 0 
	ss = 0

	for sent in doc.sents:	
		ttsent = {}
		ttsent['xml'] = etree.Element('s')
		
		ttsent['xml'].tail = '\n'
		ttsrng = range(ss, tcnt)
		ttsent['idxs'] = list(ttsrng)
		sents.append(ttsent)
		teitext.append(ttsent['xml'])
  	
		for token in sent:
			newtok = {}
			newtok['xml'] = etree.Element('tok')
			newtok['form'] = token.text
			if id in token._.custom.keys():
				tokid = token._.custom['id']
			else:
				tokid = 'w-' + str(token.i+1)
				token._.custom['id'] = tokid
			newtok['id'] = tokid
			newtok['xml'].set('id', tokid)
			for fld in spacyflds:
				satt = spacyflds[fld]
				if token.tag_:
					newtok['xml'].set('xpos', token.tag_)
				if token.pos_:
					newtok['xml'].set('upos', token.pos_)
				if token.norm_:
					newtok['xml'].set('reg', token.norm_)
				if token.lemma_:
					newtok['xml'].set('lemma', token.lemma_)
				if 'ner' in options.keys():
	 				if token.ent_iob_ and token.ent_iob_ != 'O':
 						newtok['xml'].set('ner', token.ent_iob_ + '-' + token.ent_type_)
				if token.dep_:
					newtok['xml'].set('deprel', token.dep_)
				if token.head:
					if id in token._.custom.keys():
						headid = token.head._.custom['id']
					else:
						headid = 'w-' + str(token.head.i+1)
					newtok['xml'].set('head', headid)
			tokens.append(newtok)
			if token.whitespace_: # check for nospace somehow
				newtok['xml'].tail = token.whitespace_
			newtok['xml'].text = token.text
			ttsent['xml'].append(newtok['xml'])
	
	ttobj.tokens = tokens
	ttobj.regions['s'] = sents
	
	if 'name' in options.keys():
		placeranges(ttobj, doc.ents, 'name')	
	
	return ttobj


# Place a (collection of) Spacy Spans into the XML
def placeranges(tt, ranges, elm="s"):
	if not elm in tt.regions.keys():
		tt.regions[elm] = []
	for rng in ranges:
		if not 'id' in rng[0]._.custom.keys():
			continue
		id1 = rng[0]._.custom['id']
		tok1 = tt.xml.find(".//tok[@id=\""+id1+"\"]")
		if tok1 is None:
			print('failed to find: tok[@id="'+id1+'"]')
			continue
		el1 = etree.Element(elm)
		ttxml.insertbefore(tok1, el1)
		if rng.label_:
			el1.set('label', rng.label_)
		if rng.kb_id_:
			el1.set('ref', rng.kb_id_)
		if rng.id_:
			el1.set('id', rng.id_)
		idlist = []
		for t in rng:
			idlist.append('#' + t._.custom['id'])
			last = t.i
		# Move the tail from the last token to the range (move last space outside)
		el1.tail = tt.tokens[last]['xml'].tail
		tt.tokens[last]['xml'].tail = ''
		el1.set('sameAs', " ".join(idlist))
		if str(rng) != "" and elm != "s":
			el1.set('form', str(rng))
		ttxml.moveinside(el1)
		newrange = {}
		newrange['xml'] = el1
		rng = range(rng[0].i, rng[0].i+1)
		newrange['idxs'] = list(rng)
		tt.regions[elm].append(newrange)

def writeback(xmlf, spacydoc, attrs = ""):
	toklist = {}
	doatts = attrs.split(",")
	for token in spacydoc:
		tokid = token._.custom['id']
		xtok = toklist[tokid]
		for attr in doatts:
			if attr in spacyflds.keys():
				if attr == "upos":
					val = token.pos_ 
				if attr == "reg":
					val = token.norm_ 
				if attr == "ner":
					val = token.ent_iob_ 
				if attr == "lemma":
					val = token.lemma_ 
				if attr == "deprel":
					val = token.dep_ 
			elif attr in token._.custom.keys():
				val = token._.custom[attr]
			else:
				val = ""
		if val != "" and val != "_":
			xtok.attrib[attr] = val
	# Place back the entities
	placeranges(xmlf, spacydoc.ents, 'name')
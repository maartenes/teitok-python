# TEITOK Python tools

This is a Python3 package to work with TEITOK files for NLP purposes. The main modules in the package are:

- ttxml: read and write TEITOK/XML files into a Python object
- ttspacy: convert to objects to and from the SpaCy format, so that you can run a SpaCy pipeline
- ttflair: convert to objects to and from the FLAIR format, so that you can run a FLAIR pipeline
- ttconllu: convert to objects to and from the CoNLL-U format
- ttwav: run Wav2Vec2 on audio files, and get the result as a Python object

## TTSPACY

ttspacy lets you read and write to SpaCy object, so that you can run SpaCy on TEITOK documents. It provides the following
functions:

| function | description |
| --- | --- |
| makedoc | Turn a ttxml object into a SpaCy Doc |
| fromdoc | Turn a SpaCy Doc into a ttxml doc (with limited XML) |
| writeback | Write any info added by SpaCy back into the ttxml document it came from |
| placeranges | Place a specific set of regions from SpaCy into the ttxml document (sents,ents) |

Here is a small example of how to read a TEITOK document into Python, run SpaCy on it, and write it back:

```
from teitok import ttxml
from teitok import ttspacy
import spacy
from spacy.language import Language

# Read the file myfile.xml
tt = ttxml.read('myfile.xml')

# Turn it into a SpaCy Doc
doc = ttspacy.makedoc(tt2)

# Run some process on it - in this case NER
nlp = spacy.load("en_core_web_sm")
ner = nlp.get_pipe("ner")
ner(doc)

# Place the NER into the ttxml object
ttspacy.placeranges(tt2.xml, doc.ents, 'name')

# Write the ttxml document back to disk
ttxml.save(tt, 'newfile.xml')
```

You can also create a TEITOK file from SpaCy run over plain text:

```
from teitok import ttxml
from teitok import ttspacy
import spacy
from spacy.language import Language

# Run Spacy on a text
text = "John went to school. He was very tired. He lived in Amsterdam."
nlp = spacy.load("en_core_web_sm")
doc2 = nlp(text)

# Convert the Spacy Doc to ttxml
tt3 = ttspacy.fromdoc(doc2, {'name': 1})

ttxml.save(tt, 'newfile.xml')
```

## TTWAV

ttwav is a simple wrapper to run Wav2Vec2 on WAV files. The main function is `recognize`, which 
takes the following arguments:

- audiofile: the name of the audio file to recognize (by default taken from command line argument)
- model: the name of the model - defaults to `facebook/wav2vec2-base-960h`
- longbreak: take any break longer than this value (in seconds) to start a new utterance
- capitalize: capitalize the first word of each utterance

Example code:

```
from teitok import ttwav
from teitok import ttxml

# Run ASR on the audio file given on the command line
sentences = ttwav.recognize(longbreak=1, capitalize=True)

# Create a TEI document out of the sentences
xml = ttxml.obj2tei(sentences, selm="u")

# Write the TEI document to file
ttxml.save(xml, "output.xml")
```